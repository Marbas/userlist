export interface ITableTitle {
    title: string[]
    sortFildName?: string
    order?: string
}

export interface IUser {
    _id: string
    name: string
    age: number
    email: string
    phone: string
    country: string
}
