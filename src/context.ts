import React from 'react'
import { IUser } from './interface'

type ContextProps = {
    userList: IUser[]
    setuserList: React.Dispatch<React.SetStateAction<IUser[] | []>>
    id: string
}

export const GlobalContext = React.createContext<Partial<ContextProps>>({})
