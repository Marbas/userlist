import React, { useState } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { GlobalContext } from './context'
import Main from './containers/Main'
import Details from './containers/Details'

import { data } from './fixtures'

import { ITableTitle, IUser } from './interface'

function App() {
    const [userList, setuserList] = useState<IUser[] | []>(data)
    const header: ITableTitle = {
        title: ['Action', 'Name', 'Age', 'Country', 'Email', 'Phone'],
    }

    return (
        <GlobalContext.Provider value={{ userList, setuserList }}>
            <Router>
                <Switch>
                    <Route exact path="/">
                        <Main columnList={header} userList={userList} />
                    </Route>
                    <Route exact path="/details/:id">
                        <Details />
                    </Route>
                </Switch>
            </Router>
        </GlobalContext.Provider>
    )
}

export default App
