import React from 'react'
import { makeStyles } from '@material-ui/styles'
import { ITableTitle } from '../../interface'

const useStyles = makeStyles({
    head: {
        display: 'flex',
    },
    cell: {
        display: 'flex',
        flexWrap: 'wrap',
        wordBreak: 'break-word',
        flex: '1 2 auto',
        width: '0',
    },
})

export default function TableHeader({ columnList }: { columnList: ITableTitle }) {
    const classes = useStyles()

    return (
        <div className={classes.head}>
            {columnList.title.map((title, index) => (
                <h3 key={index} className={classes.cell}>
                    {title}{' '}
                </h3>
            ))}
        </div>
    )
}
