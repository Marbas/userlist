import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles({
    row: {
        display: 'flex',
        flexDirection: 'column',
        margin: '16px 32px',
    },
})

interface ISelect {
    title: string
    country: string
    onChange: (value: React.ChangeEvent<HTMLSelectElement>) => void
}

export default function Select({ title = '', country, onChange }: ISelect) {
    const classes = useStyles()
    const [optionList, setOptionsList] = useState<JSX.Element[] | []>([])

    useEffect(() => {
        const countryList: string[] = ['Australia', 'Russia', 'USA']
        const baseOptionList = countryList.map((currCountry, index) => (
            <option key={index} value={currCountry}>
                {currCountry}
            </option>
        ))
        if (!countryList.find(currCountry => currCountry === country)) {
            setOptionsList([
                <option key={baseOptionList.length++} value={country}>
                    {country}
                </option>,
                ...baseOptionList,
            ])
        } else {
            setOptionsList(baseOptionList)
        }
    }, [country])

    return (
        <div className={classes.row}>
            <span>{title}</span>
            <select onChange={e => onChange(e)} value={country}>
                {optionList}
            </select>
        </div>
    )
}
