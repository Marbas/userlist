import React from 'react'
import { makeStyles } from '@material-ui/styles'
import { ITableTitle, IUser } from '../../interface'
import TableBody from '../TableBody'
import TableHeader from '../TableHeader'

const useStyles = makeStyles({
    table: {
        display: 'flex',
        flexDirection: 'column',
        margin: '32px 0',
        backgroundColor: 'white',
        borderRadius: '5px',
        width: '100%',
        padding: '24px',
        overflowY: 'auto',
    },
})

interface ITableProps {
    columnList: ITableTitle
    userList: IUser[]
}

export default function Table({ columnList, userList }: ITableProps) {
    const classes = useStyles()
    return (
        <div className={classes.table}>
            <TableHeader columnList={columnList} />
            <TableBody userList={userList} />
        </div>
    )
}
