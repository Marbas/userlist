import React from 'react'
import { makeStyles } from '@material-ui/styles'
import { IUser } from '../../interface'
import TableRow from '../TableRow'

interface ITableBody {
    userList: IUser[]
}

const useStyles = makeStyles({
    body: {
        display: 'flex',
        flexDirection: 'column',
    },
})

export default function TableBody({ userList }: ITableBody) {
    const classes = useStyles()
    return (
        <div className={classes.body}>
            {userList?.map(row => (
                <TableRow key={row._id} row={row} />
            ))}
        </div>
    )
}
