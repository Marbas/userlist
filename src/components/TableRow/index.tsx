import React, { useContext } from 'react'
import { useHistory } from 'react-router'
import { makeStyles } from '@material-ui/styles'
import { IUser } from '../../interface'
import { GlobalContext } from '../../context'

interface ITableRow {
    row: IUser
}

const useStyles = makeStyles({
    row: {
        display: 'flex',
        flexDirection: 'row',
        border: '1px solid blue',
        borderRadius: '4px',
        marginBottom: '8px',
        alignItems: 'center',
    },
    cell: {
        display: 'flex',
        flexWrap: 'wrap',
        wordBreak: 'break-word',
        flex: '1 2 auto',
        width: '0',
    },
})

export default function TableRow({ row }: ITableRow) {
    const classes = useStyles()
    const { setuserList } = useContext(GlobalContext)
    const history = useHistory()

    const handleDeletItem = (id: string) => {
        setuserList?.(userList => userList?.filter(item => item._id !== id))
    }

    const handleLink = (id: string) => {
        history.push(`/details/${id}`)
    }

    return (
        <div className={classes.row}>
            <div className={classes.cell}>
                <button onClick={() => handleDeletItem(row._id)}>Del</button>
                <button onClick={() => handleLink(row._id)}>Edit</button>
            </div>
            <div className={classes.cell}>{row.name}</div>
            <div className={classes.cell}>{row.age}</div>
            <div className={classes.cell}>{row.country}</div>
            <div className={classes.cell}>{row.email}</div>
            <div className={classes.cell}>{row.phone}</div>
        </div>
    )
}
