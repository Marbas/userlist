import React from 'react'
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles({
    row: {
        display: 'flex',
        flexDirection: 'column',
        margin: '16px 32px',
    },
})

interface IInput {
    title: string
    type?: string
    value?: string | number
    onChange: ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => void
}

export default function Input({ title = '', type = 'text', value, onChange }: IInput) {
    const classes = useStyles()
    return (
        <div className={classes.row}>
            <span>{title}</span>
            <input type={type} value={value} onChange={onChange} />
        </div>
    )
}
