import React, { useContext } from 'react'
import { makeStyles } from '@material-ui/styles'
import { useParams, useHistory } from 'react-router-dom'
import Input from '../../components/Input'
import Select from '../../components/Select'
import { IUser } from '../../interface'
import { GlobalContext } from '../../context'

const useStyles = makeStyles({
    details: {
        display: 'flex',
        flexDirection: 'column',
        height: '100vh',
        maxWidth: '1024px',
        margin: '0 auto',
    },
    row: {
        margin: '16px 32px',
    },
})

interface IOption {
    label: string
    value: string
}

export default function Details() {
    const classes = useStyles()
    const { userList, setuserList } = useContext(GlobalContext)
    const history = useHistory()
    const { id } = useParams()

    const newList: IUser[] = [...userList!]
    const index = newList?.findIndex(row => row._id === id)
    const user = newList?.find(row => row._id === id)

    const handleChangeName = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
        newList[index].name = value
        setuserList!(newList)
    }
    const handleChangePhone = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
        newList[index].phone = value
        setuserList!(newList)
    }
    const handleChangeEmail = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
        newList[index].email = value
        setuserList!(newList)
    }
    const handleChangeAge = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
        newList[index].age = +value
        setuserList!(newList)
    }

    const handleChangeContry = ({
        currentTarget: { value },
    }: React.ChangeEvent<HTMLSelectElement>) => {
        newList[index].country = value
        setuserList!(newList)
    }

    return (
        <>
            <button onClick={() => history.goBack()}>Back</button>
            <div className={classes.details}>
                <Input title="name" value={user!.name} onChange={handleChangeName} />
                <Input title="age" type="number" value={user!.age} onChange={handleChangeAge} />
                <Select
                    title="country"
                    country={user!.country}
                    onChange={e => handleChangeContry(e)}
                />
                <Input title="phone" value={user!.phone} onChange={handleChangePhone} />
                <Input title="email" value={user!.email} onChange={handleChangeEmail} />
            </div>
        </>
    )
}
