import React from 'react'
import { makeStyles } from '@material-ui/styles'
import Table from '../../components/Table'
import { ITableTitle, IUser } from '../../interface'

const useStyles = makeStyles({
    main: {
        display: 'flex',
        height: '100vh',
        maxWidth: '1024px',
        margin: '0 auto',
    },
})

interface IMain {
    columnList: ITableTitle
    userList: IUser[]
}

export default function Main({ columnList, userList }: IMain) {
    const classes = useStyles()
    return (
        <div className={classes.main}>
            <Table columnList={columnList} userList={userList} />
        </div>
    )
}
